This is a blank `revealjs` presentation with my best current configuration options. Fork/clone this repository to make a new talk.

```
git clone --recurse-submodules {path to this repo}
```

To make an auto-reloading web server simply call `httpwatcher`.
